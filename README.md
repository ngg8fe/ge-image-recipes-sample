# Raspberry Pi - Apertis Custom Image Recipes

This repository contains Apertis recipes customized for Raspberry Pi.

Based on [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
official repository. Check it out for further documentation on Apertis recipes.

## Available recipes

### ospack-rpi64_containers.yaml

Build platform-independent minimal (headless system) ospack, including extra
tools for development purposes, docker engine and docker-compose.

Based on [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
`ospack-minimal.yaml` original recipe.

```
debos --scratchsize=10G ospack-rpi64_containers.yaml
```

### image-rpi64.yaml

Build platform-specific image for Raspberry Pi using a previously built ospack
(rpi64\_containers by default).

```
debos --scratchsize=10G image-rpi64.yaml
```

## Custom OBS/repositories

The custom images recipes might pull certain packages from extra repositories
made specifically to this purpose.

### apertis:demo:rpi64-containers

Packages backported and configured to Apertis v2021 for Raspberry Pi 64:

* OBS instance: https://build.collabora.co.uk/project/show/apertis:demo:rpi64-containers
* Apertis repository: https://repositories.apertis.org/rpi64-containers/dists/v2021/rpi64-containers/
